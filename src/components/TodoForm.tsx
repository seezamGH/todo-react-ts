import React, { useState } from 'react'

interface TodoFormProps {
  onAdd(title: string): void
}

export const TodoForm: React.FC<TodoFormProps> = ({onAdd}) => {
  const [title, setTitle] = useState<string>('')

  const changeHndler = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setTitle(event.target.value)
  }
  const keyPressHandler = (event: React.KeyboardEvent): void => {
    if(event.key === 'Enter') {
      onAdd(title)
      setTitle('')
    }
  }

  return (
    <div className='input-field mt2'>
      <input
        type='text'
        id='title'
        placeholder='Введите название дела'
        value={title}
        onKeyPress={keyPressHandler}
        onChange={changeHndler}
      />
      <label htmlFor='title' className='active'>
        Введите название дела
      </label>
    </div>
  )
}
