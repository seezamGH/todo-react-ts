import React, { MouseEvent } from 'react';
import { Itodo } from './interfaces';

type TodoListProps = {
  todos: Itodo[];
  onToggle(id: number): void; // можно и так
  onRemove: (id: number) => void; // и так
};

export const TodoList: React.FC<TodoListProps> = ({
  todos,
  onRemove,
  onToggle
}) => {
  const removeHandler = (event: React.MouseEvent, id: number) => {
    event.preventDefault();
    onRemove(id);
  };

  if (todos.length === 0) {
    return <p className='center'>Пока дел нет!</p>;
  }
  return (
    <ul>
      {todos.map(todo => {
        const classes: string[] = ['todo'];

        if (todo.complited) {
          classes.push('complited');
        }
        return (
          <li key={todo.id} className={classes.join(' ')}>
            <label>
              <input
                type='checkbox'
                checked={todo.complited}
                onChange={() => onToggle(todo.id)}
              />
              <span>{todo.title}</span>
              <i
                className='material-icons red-text'
                onClick={event => removeHandler(event, todo.id)}
              >
                delete
              </i>
            </label>
          </li>
        );
      })}
    </ul>
  );
};
