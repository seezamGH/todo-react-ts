import React, { useState } from 'react';
import Navbar from './components/Navbar';
import { TodoForm } from './components/TodoForm';
import { TodoList } from './components/TodoList';
import { Itodo } from './components/interfaces';

const App: React.FC = () => {
  const [todos, setTodos] = useState<Itodo[]>([]);

  const addHandler = (title: string) => {
    if (title) {
      const newTodo = {
        title,
        id: Date.now(),
        complited: false
      };
      setTodos(prevState => [newTodo, ...prevState]);
    }
    console.log(title);
  };

  const toggleHandler = (id: number): void => {
    setTodos(prevState =>
      prevState.map(todo => {
        if (todo.id === id) {
          todo.complited = !todo.complited;
        }

        return todo;
      })
    );
  };

  const removeHandler = (id: number): void => {
    const shouldRemove = window.confirm(
      'Вы действительно хотите удалить дело?'
    );
    if (shouldRemove) {
      setTodos(prevState => prevState.filter(todo => todo.id !== id));
    }
  };

  return (
    <>
      <Navbar />
      <div className='container'>
        <TodoForm onAdd={addHandler} />
        <TodoList
          todos={todos}
          onToggle={toggleHandler}
          onRemove={removeHandler}
        />
      </div>
    </>
  );
};

export default App;
